/*global document: true*/

(function () {
    var code_blocks = document.querySelectorAll('pre code');

    Array.prototype.forEach.call(code_blocks, function (code_block) {
        var lines = code_block.innerText.split('\n').slice(1),
            min_indent_level = Infinity;

        lines.forEach(function (line) {
            var match = line.match(/^\s+/);

            if (match) {
                min_indent_level = Math.min(min_indent_level, match[0].length);
            }
        });

        console.log('min indent: ' + min_indent_level);

        code_block.innerText = lines.map(function (line) {
            return line.substr(min_indent_level);
        }).join('\n');

    });
})();
